<a name="H9sKD"></a>
# 辅学院企业培训系统
**辅学院是岳琛科技团队倾力打造的一款企业培训系统**，旨在助力企业构建高效、专业的团队。该系统提供了一站式、个性化的培训解决方案，以满足企业多样化的培训需求。<br />辅学院基于Java和MySQL进行开发，采用前后端分离的模式，前端核心框架为Vue2，后端核心框架为SpringBoot3，确保了系统的稳定性和高效性。开源版本已经提供了丰富的功能，包括上传课件、创建课程、在线视频和文档学习，以及学员进度追踪等，为企业提供了基础的培训支持。<br />**针对企业级培训场景，我们特别推出了功能更全面、响应更迅速、并发能力更强的企业版本。这个版本不仅支持在线培训和线下培训，还融入了线上考试、练习考试和在线调研等多种学习方式，使得培训形式更加多样化和灵活。同时，我们加强了系统的安全防护，包括视频转码加密、防盗链、学习防快进和考试防作弊等措施，确保培训内容的安全性和完整性。**<br />**为了方便企业快速部署和集成，辅学院还与企业微信、钉钉等主流办公系统进行了深度集成。这使得企业能够轻松地将培训平台与日常办公流程相结合，提高培训效果和员工参与度。**<br />总之，辅学院以其先进的技术架构、丰富的功能特点和良好的集成性，成为企业打造高效、专业团队的得力助手。 无论是大型企业还是中小型企业，辅学院都能为其提供优质的培训解决方案。
<a name="ac8f07b0"></a>
## 开源项目详情链接
| [https://gitee.com/auxiliary-college-enterprise/open-source-training-system](https://gitee.com/auxiliary-college-enterprise/open-source-training-system) |
| --- |

<a name="tQAEi"></a>
## 安装部署
| **技术栈** | 1、VUE2<br />2、node v14.21.3                                                                                                                                          |
| --- |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **部署文档** | [https://blog.csdn.net/yuechenlearn/article/details/139412656?spm=1001.2014.3001.5502](https://blog.csdn.net/yuechenlearn/article/details/139412656?spm=1001.2014.3001.5502) 《辅学院开源版部署文档》 |
| **企业版支持** | 1、钉钉、企业微信**自建应用**部署方式<br />2、钉钉、企业微信**第三方应用**部署方式（**附：安全管理制度和应急预案和流程**）<br />3、**100%源码交付**，企业可自由拓展功能，满足不同培训场景的需求。                                                                                                                                                                    |
| **官方网站** | [https://yuechenedu.cn/](https://yuechenedu.cn/)                                                                                                                                                                                                                        |
<a name="K63Qv"></a>
## 系统概览
![171741272ew12.png](https://yuechenlearntrain.oss-cn-hangzhou.aliyuncs.com/xuanchuan/homeer.png)
![171743272ew12.png](https://yuechenlearntrain.oss-cn-hangzhou.aliyuncs.com/xuanchuan/manage.png)
![1717432qw0612.png](https://yuechenlearntrain.oss-cn-hangzhou.aliyuncs.com/xuanchuan/yuechenedu.gif)

<a name="431b259b"></a>
## 企业版网页体验链接
| 管理员端地址 | [https://app135926.eapps.dingtalkcloud.com/dist/index](https://app135926.eapps.dingtalkcloud.com/dist/index) |
| --- | --- |
| PC学员端地址 | [https://app135926.eapps.dingtalkcloud.com/pcuser/home](https://app135926.eapps.dingtalkcloud.com/pcuser/home) |
| H5学员端地址 | [https://app135926.eapps.dingtalkcloud.com/mobile/index](https://app135926.eapps.dingtalkcloud.com/mobile/index) |


<a name="431b259b"></a>
## 企业版页面展示
![输入图片说明](static/image/admin-page.png)



<a name="Xvji0"></a>
## <br />使用协议
● 要求

- 保留页脚处版权信息。
- 保留源代码中的协议。
- 如果修改了代码，则必须在文件中进行说明。

<a name="K53Qv"></a>
## 官方交流群（加群获得产品设计技术支持和配置文档）
![1717432720612.png](https://yuechenlearntrain.oss-cn-hangzhou.aliyuncs.com/xuanchuan/qun1.png)